from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser
import sys
import os


#Sqrt of # of flows
FLOWS = 17
# Calibration of Buffer
BUFFER_CALIBRATION = FLOWS * 1.4

# Number of samples to skip for reference util calibration.
CALIBRATION_SKIP = 10

# Number of samples to grab for reference util calibration.
#CALIBRATION_SAMPLES = 30
CALIBRATION_SAMPLES = 30

# Set the fraction of the link utilization that the measurement must exceed
# to be considered as having enough buffering.
TARGET_UTIL_FRACTION = 0.991

# Fraction of input bandwidth required to begin the experiment.
# At exactly 100%, the experiment may take awhile to start, or never start,
# because it effectively requires waiting for a measurement or link speed
# limiting error.
START_BW_FRACTION = 0.98

# Number of samples to take in get_rates() before returning.
NSAMPLES = 3

# Time to wait between samples, in seconds, as a float.
SAMPLE_PERIOD_SEC = .8

# Time to wait for first sample, in seconds, as a float.
SAMPLE_WAIT_SEC = 3.0

def get_txbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))
    # Extract TX bytes from:
    #Inter-|   Receive                                                |  Transmit
    # face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
    # lo: 6175728   53444    0    0    0     0          0         0  6175728   53444    0    0    0     0       0          0
    return float(line.split()[9])



def get_rates(iface, nsamples=NSAMPLES, period=SAMPLE_PERIOD_SEC,
              wait=SAMPLE_WAIT_SEC):
    """Returns rate in Mbps"""
    # Returning nsamples requires one extra to start the timer.
    nsamples += 1
    last_time = 0
    last_txbytes = 0
    ret = []
    sleep(wait)
    while nsamples:
        nsamples -= 1
        txbytes = get_txbytes(iface)
        now = time()
        elapsed = now - last_time
        last_time = now
        rate = (txbytes - last_txbytes) * 8.0 / 1e6 / elapsed
        if last_txbytes != 0:
            # Wait for 1 second sample
            ret.append(rate)
        last_txbytes = txbytes
        print '.',
        sys.stdout.flush()
        sleep(period)
    return ret
	

def set_q(iface, q):
    "Change queue size limit of interface"
    cmd = ("tc qdisc change dev %s root netem limit %s" % (iface, q))
    os.system(cmd)
	
def median(l):
    "Compute median from an unsorted list of values"
    s = sorted(l)
    if len(s) % 2 == 1:
        return s[(len(l) + 1) / 2 - 1]
    else:
        lower = s[len(l) / 2 - 1]
        upper = s[len(l) / 2]
        return float(lower + upper) / 2

		
def set_q(iface, q):
    "Change queue size limit of interface"
    cmd = ("tc qdisc change dev %s root netem limit %s" % (iface, q))
    os.system(cmd)
	
def ok(fraction):
    "Fraction is OK if it is >= args.target"
    return fraction >= TARGET_UTIL_FRACTION


def do_sweep(iface):
    """Sweep queue length until we hit target utilization.
       We assume a monotonic relationship and use a binary
       search to find a value that yields the desired result"""

    bdp = (62500.0 * 82) / 8.0 / 3
    run = 1
    # nflows = args.nflows * (args.n - 1
    min_q, max_q = 1, int(bdp)

    print "\nSetting q=%d " % max_q
    sys.stdout.flush()
    set_q(iface, max_q)
	
    # Wait till link is 100% utilised and train 
    sleep(5)	
    reference_rate = reference_rate1(iface)
	
    queue_len = mod_buf(iface,min_q, max_q,reference_rate)
    while  queue_len <= (bdp / BUFFER_CALIBRATION) and run <= 5:
           run +=1
           min_q, max_q = 1, int(bdp)
           queue_len = mod_buf(iface,min_q, max_q,reference_rate)
			
    print "*** Final Min_queue for target: %d" % queue_len
    return queue_len
			
def reference_rate1(iface):
    reference_rate = 0.0
    while reference_rate <= 63.0 * START_BW_FRACTION:
        rates = get_rates(iface, nsamples=CALIBRATION_SAMPLES+CALIBRATION_SKIP)
        print "measured calibration rates: %s" % rates
        # Ignore first N; need to ramp up to full speed.
        rates = rates[CALIBRATION_SKIP:]
        reference_rate = median(rates)
	reference_rate += 0.75 
        ru_max = max(rates)
	ru_min = min(rates)
        #ru_stdev = stdev(rates)
        print "Reference rate median: %.3f max: %.3f min: %.3f" %(reference_rate, ru_max, ru_min)
        sys.stdout.flush()
	return reference_rate
			
	
def mod_buf(iface,min_q, max_q,reference_rate):
	
    while abs(min_q - max_q) >= 2:
        mid = (min_q + max_q) / 2
        print "Trying q=%d  [%d,%d] " % (mid, min_q, max_q),
        sys.stdout.flush()
        set_q(iface, mid)
        sleep(10)
              
        s_rates = get_rates(iface,nsamples=6)
        med_rate = median(s_rates)
        print "Median rate is %f" % med_rate
        throughput_fraction = med_rate * 1. / reference_rate
        throughput_fraction = float(round(throughput_fraction,3))      

	print '< Utilization:%f > ' % (throughput_fraction)
        sys.stdout.flush()

        if ok(throughput_fraction):
            max_q = mid
        else:
            min_q = mid

    #monitor.terminate()
    print "*** Min_queue for target: %d" % max_q
    return max_q
	
def main():
	
	queue_len = do_sweep('eth2')
	output = "%.5f" % (queue_len)
    #open("/result.txt", "a").write(output)
	
if __name__ == '__main__':
    main()

