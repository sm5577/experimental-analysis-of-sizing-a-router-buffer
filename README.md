# Introduction #
This experiment deals with the **Experimental Analysis of Sizing a Router Buffer**^.

^*G. Appenzeller, I. Keslassy, and N. McKeown. Sizing
router buffers. In SIGCOMM ’04, pages 281–292, New
York, NY, USA, 2004. ACM Press.*


Time for Reproducing the results:
The total experiment took around 10 hours of simulation time (including for calibration errors and outliers)

Average time for a single run takes less than 8 minutes.


### Background ###

The general rule of thumb for a standard buffer size for a router is RTT*C , where RTT is the average round trip time and C is the bottleneck link capacity.The sizing Router buffer paper by Appenzeller et al.^ outlines a new values for the buffer size depending on the number of flows (N) passing through the bottleneck link.The authors showed that the new buffer size is RTT * C /√(N) through a series of simulations in NS2 for various % of link utilization.

The graph that we are going to replicate is as shown below

![sigcomm_result.png](https://bitbucket.org/repo/BRjkoM/images/2284886959-sigcomm_result.png)

All network routers use a buffer so as to maintain a good throughput on the link.The buffer size varies accordingly with TCP congestion control algorithm and manufactures generally install very large buffers so as avoid packet drop when a bottleneck occurs, however in practical large networks using huge buffer sizes causes various problems like bufferbloat etc. Finding the optimum buffer size is an important factor which affects the throughput and the cost of a router (high speed memory is costly) 


### Results ###

 ![Sizing Router Buffer.PNG](https://bitbucket.org/repo/BRjkoM/images/4247869856-Sizing%20Router%20Buffer.PNG)

Reference Transfer Rates

The transfer rates between the client and server was found to be quite unstable and not an expected value as specified in the GENI RSPEC. The data between client and server had a fluctuating rate throughout the experiment. A queuing discipline such as tbf was not used so as to make the experiment as close to a real network. Instead of making changes to the qdisc, a reference rate was selected by sampling a number of transfer rates per second and the median of those values was assigned as a reference rate and used for calculating the link utilization for different buffer size values.

Buffer Sweep Algorithm

A binary search algorithm is used to find the optimum buffer size. Initial buffer size is set at RTT X C and the link utilization is found and verified with the desired value. For each queue size, the throughput is measured and the next buffer size is chosen, which has the average of the smallest buffer size that was above-threshold (which maintained “full” rate), and the largest buffer size that was below threshold (that did not maintain “full” rate). Binary search algorithm was used because it is simple and fast to close in on an unknown target value.

Experiment Calibrations

Many parameters required calibrations for obtaining a steady values, this was due to the fluctuating rates of the link. For measuring the initial reference rate, a CALIBRATION_SAMPLES count was used which determines the number of samples to be taken before taking their median value. Similarly sample wait period was used to set wait-time between samples. 
After multiple trial-errors, the transfer rate was found to be very slightly higher than the median rate, a normalizing calibration was used on the median value to obtain satisfactory results for different number of flows, which surprisingly was found to be a constant throughout the experiment.
Due to the sensitivity of the binary search algorithm, a single bad measurement can result in completely offside results (outliers), various other calibrations were used to reinforce the binary search algorithm.

Analysis

Over 120 simulations are run in order to achieve the expected results, most of them were initial trials to find the optimum calibration parameters, once these parameters were found, the results started behaving as expected and the experiment outcomes became predictable. Though some of the results did turn out to be outliers, which were identified and excluded from the end result. The final results were averaged over five times for each different value of flows and the average was plotted.

We have compared our experimental results with the NETFPGA results from the paper Experimental study of router buffer sizing.  N. Beheshti, Y. Ganjali, M. Ghobadi, N. McKeown, and G. Salmon.  IMC 2008 and the standard Analytical results. Fig. 3 shows the minimum required buffer for a given minimum utilization for different number of flows. Until 400 flows the experimental results are above the analytical minimum, this could be the fluctuations in rates.

The model predicted by Appenzeller et al. holds well when there are large number of flows, beyond 500 flows, the experiment required even less amount of buffer to maintain 99% throughput, this could be attributed to the desynchronized flows caused due to the variations in delay and transfer rates. 

The link utilization at 98% requires less amount of buffer than when at 99% utilization, though above 500 flows both seem to converge.

Overall CPU utilization of the VMs was well below 10% even when running 800 parallel iperf sessions.


### Run My Experiment ###

* Reserve resources on GENI using the RSPECs provided in the source.
* The typical topology for running this experiment is as shown below

![topo_snap.JPG](https://bitbucket.org/repo/BRjkoM/images/1697274621-topo_snap.JPG)

* The links between Client and Router are 800 Mbps and the links between server and Router are 62.5 Mpbs
* After successfully reserving the resources, SSH into the VMs and bring up the respective *eth* interfaces.

Client : eth1 10.10.1.1   
 
Router : eth1 10.10.1.2
         eth2 10.10.3.3

Server1 : eth1 10.10.3.1

Server2 : eth2 10.10.3.2

Gateway : 255.255.255.0

* The bottleneck link is on the eth2 interface of the router.

*Note: Please make sure that the bottleneck link is 'eth2'*

* Setup the routes between the Vms using the following

Router

```
#!Shell

sudo sysctl -w net.ipv4.ip_forward=1 
```


Client 1

```
#!Shell

sudo route add -host 10.10.3.2 gw 10.10.1.2 dev eth1
sudo route add -host 10.10.3.1 gw 10.10.1.2 dev eth1
```

server 1

```
#!Shell

sudo route add -host 10.10.1.1 gw 10.10.3.3 dev eth1
```


server 2

```
#!shell

sudo route add -host 10.10.2.1 gw 10.10.3.3 dev eth1
```

* Next set the delay between the client and servers, run the below commands on server1 and server2 nodes

```
#!python

sudo tc qdisc add dev eth1 root netem delay 80ms
```

* Run the experiment as root using `sudo su` on all VMs.

**For flows 25-250**

* Install iperf3 on Client and Server nodes using

```
#!python

apt-get update
apt-get install iperf3
```

* Select the number of flows (say 25) that you want to run ,On the router,save the python code from the source folder into a file `bsweep_test.py`
*Note: Make sure there are no indentation errors.*

* Run iperf3 on both the servers using the command

```
#!python

iperf3 -s -V &>server1_25f_1.log
iperf3 -s -V &>server2_25f_1.log
```


* Open two terminals of the Client node and start sending iperf3 traffic to both the servers.

```
#!python

iperf3 -c 10.10.3.2 -t 3600 -i 10 -P 25 &> client2_25f_1.log
iperf3 -c 10.10.3.1 -t 3600 -i 10 -P 25 &> client1_25f_1.log
```

* **Wait for 10 seconds** and start the `bsweep_test.py` on the router node.
* Initially the reference rate is measured and displayed on the console, make a note of it. Next the buffer is set and a binary search algorithm is used to find the optimum buffer length.
* Due to the fluctuations in transfer rates, the link utilization % fluctuates as well, the binary search is modified to run untill a optimum value is obtained.
* Before starting the next set of rounds, kill the iperf3 process (use `ps` command to view the processes) on the servers using `kill -9 "PID"`

* The log files contain the transmission rates of the iperf data.

* Repeat the experiment for different number of flows using the different python codes provided.
* Maximum number of parallel flows possible using iperf3 is 128. 

**For flows 300-800**

This part of the experiment requires additional setup of installing a "modified" version of the iperf. Follow the below steps for the installation.

* Run this setup on the client and server nodes

```
#!python

sudo su
apt-get update
apt-get install build-essential
wget http://downloads.sourceforge.net/project/iperf/iperf-2.0.5.tar.gz
tar xzvf iperf-2.0.5.tar.gz
cd iperf-2.0.5/src
```

* Next save the below code to a `iperf-2.0.5-wait-syn.patch`file

```
#!python


diff --git a/Client.cpp b/Client.cpp
index 71a5b5c..6aa25f8 100644
--- a/Client.cpp
+++ b/Client.cpp
@@ -213,6 +213,7 @@ void Client::Run( void ) {
 
 #if HAVE_THREAD
     if ( !isUDP( mSettings ) ) {
+    sleep(5);  // Sleep a bit before blasting data
 	RunTCP();
 	return;
     }
diff --git a/Listener.cpp b/Listener.cpp
index 94e32de..0071a59 100644
--- a/Listener.cpp
+++ b/Listener.cpp
@@ -338,7 +338,7 @@ void Listener::Listen( ) {
     // listen for connections (TCP only).
     // default backlog traditionally 5
     if ( !isUDP( mSettings ) ) {
-        rc = listen( mSettings->mSock, 5 );
+        rc = listen( mSettings->mSock, 1024 );
         WARN_errno( rc == SOCKET_ERROR, "listen" );
     }
 
 
```

* Run the patch

```
#!python

patch -p1 < iperf-2.0.5-wait-syn.patch
```

* Build the package and install the application, running the below commands sequentially.

```
#!python

./configure
make
make install
```

* After installation use the command iperf -v to find the version.
* Run the experiment for different flows ranging from 300 -800, note down down the median rate and buffer size values.
* Eliminate the outliers and plot a graph between them to find the trend.


*Important - In order to maintain the experiment as close to the real TCP traffic, the bursts and window sizes were left unregulated and calibrations tuning was done.This, combined with the sensitivity of binary search, the obtained results will contain outliers which need to be weeded out using good judgement.


### Notes ###

* Operating System - Ubuntu
  Kernel Version - 3.13.0-68-generic
* GENI Elements
  VM - EG VM
  Aggregate - OSF Exogeni
* Software - iperf3 , iperf2 (custom)
* Comments on GENI
The experiment had to be scaled down in order to perform it in GENI. Many trials were with different combinations for achieving the desired topology however GENI failed to reserve / configure successfully.